package com.customWidget.elieself;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.elievinet.elieself.R;
import com.model.elieself.Menu;
import com.model.elieself.Produit;

import java.util.ArrayList;
import java.util.List;

/**
 * The type List view adapter.
 * Il permet un affichage personnalisé dans le listview pour chaque item
 */
public class ListViewAdapter extends ArrayAdapter<Produit> implements View.OnClickListener {
    private int ress;
    private final String NOM_REPAS_MIDI = "DEJEUNER";
    private final String NOM_REPAS_SOIR = "DINER";

    /**
     * Instantiates a new List view adapter.
     *
     * @param context  the context of application
     * @param resource the resource is the layout for every item
     * @param objects  the objects is a list which contain some produit objects
     */
    public ListViewAdapter(@NonNull Context context, int resource,  @NonNull List<Produit> objects) {
        super(context, resource, objects);
        this.ress = resource;
    }


    @NonNull
    @Override
    public View getView(int position,  @Nullable View convertView, @NonNull ViewGroup parent) {
        String name  = getItem(position).getDesignation();
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        convertView = layoutInflater.inflate(ress,parent,false);

        TextView textView = convertView.findViewById(R.id.textView2);

        if (name.toUpperCase() == NOM_REPAS_MIDI || name.toUpperCase() == NOM_REPAS_SOIR){
           convertView.setBackgroundResource(R.drawable.border_and_background);
        }
        
        textView.setText(name);

        return convertView;
    }

    @Override
    public void onClick(View v) {

    }
}

