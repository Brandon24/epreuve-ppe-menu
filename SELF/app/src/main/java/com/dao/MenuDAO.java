package com.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.database.elieself.DatabaseHelper;
import com.date.elieself.CustomDate;
import com.model.elieself.Menu;
import com.model.elieself.Produit;
import com.model.elieself.Repas;

import java.util.List;

/**
 * The type Menu dao.
 *
 */
public class MenuDAO extends DatabaseHelper {

    /**
     * Instantiates a new Menu dao.
     *
     * @param context the context
     */
    public MenuDAO(Context context) {
        super(context);
    }

    /**
     * Add menu to database.
     *
     * @param menu the menu
     */
    public void addMenuToDatabase(List<Menu> menu) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        for (Menu menu1 : menu) {
            for (Repas repas: menu1.getRepas()) {
                for (Produit produit : repas.getProduit()) {
                    values.put("semaine", CustomDate.getCurrentWeek()); // Contact date semaine
                    values.put("jour", repas.getJour()); // Contact Name
                    values.put("type_produit", produit.getTypeProduit()); //  accompagnement plat
                    values.put("type_repas", repas.getTypeRepas()); // soir midi 1 / 0
                    values.put("designation", produit.getDesignation()); // Contact Phonep
                    values.put("description",  produit.getDescription()); // Contact Phonep
                    values.put("bio",  produit.getBio()); // Contact Phonep
                    values.put("maison",  produit.getMaison()); // Contact Phonep
                    values.put("local",  produit.getLocal()); // Contact Phonep
                    values.put("frais",  produit.getFrais()); // Contact Phonep
                    db.insert("menu", "0", values);

                }
            }

        }
        db.close(); // Closing database connection
    }

}
