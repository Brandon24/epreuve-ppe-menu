package com.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.database.elieself.DatabaseHelper;
import com.date.elieself.CustomDate;
import com.model.elieself.Produit;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>The type Produit dao.</h1>
 *
 */
public class ProduitDAO extends DatabaseHelper {
    /**
     * Instantiates a new Produit dao.
     *
     * @param context the context
     */
    public ProduitDAO(Context context) {
        super(context);
    }

    /**
     * Gets all produit for one day with specific type produit and type repas.
     *
     * @param type_repas   le type de repas dejeuner ou diner
     * @param jour         the day
     * @param type_produit  le type de produit accompagnement plat dessert
     * @return tous les produits d'un jour avec le type produit(accompagnement,dessert..) et le type repas (dejeuner ou diner)
     */
    public List<Produit> getAllProduitForOneDayWithSpecificTypeProduitAndTypeRepas(String type_repas, String jour, String type_produit) {
        List<Produit> produitList = new ArrayList<Produit>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_MENU + " WHERE `semaine` = ?  and `type_repas` = ? and jour = ? and `type_produit` = ?" ;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, new String [] {CustomDate.getCurrentWeek(),type_repas,jour,type_produit});

//        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                Produit produit = new Produit(
                        cursor.getString(cursor.getColumnIndex("designation")), // type soir midi
                        cursor.getString(cursor.getColumnIndex("type_produit")), // designation
                        cursor.getString(cursor.getColumnIndex("description")), // plat entree
                        cursor.getInt(cursor.getColumnIndex("bio")), // midi soir
                        cursor.getInt(cursor.getColumnIndex("maison")),
                        cursor.getInt(cursor.getColumnIndex("local")),
                        cursor.getInt(cursor.getColumnIndex("frais"))
                );

                produitList.add(produit);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return contact list
        return produitList;
    }

}
