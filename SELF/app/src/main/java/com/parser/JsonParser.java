package com.parser;

import android.util.Log;

import com.model.elieself.Menu;
import com.model.elieself.Produit;
import com.model.elieself.Repas;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * The type Json parser.
 */
public class JsonParser {

    /**
     * Get menus from json list.
     *
     * @param json                 the json
     * @param listToAddSomeContent the list to add some content
     * @return la liste de tous les menus de la semaine
     */
    public static  List<Menu> getMenusFromJson(String json, List<Menu> listToAddSomeContent){
        String [] daysOfTheWeek= {"lundi","mardi","mercredi","jeudi","vendredi"};
        String [] typeRepas = {"dejeuner","diner"};
        String [] typeProduit = {"entree","plat","accompagnement","laitage","dessert"};

        try {
            JSONObject baseJson = new JSONObject(json);

            for (int i = 0; i < daysOfTheWeek.length; i++) {
                JSONObject dayObject = baseJson.getJSONObject(daysOfTheWeek[i]);
                Menu menu = new Menu();
                menu.setSemaine(baseJson.getString("date_debut"));
                Log.i("DAY",daysOfTheWeek[i]);

                for (int j = 0; j < typeRepas.length ; j++) {
                    JSONObject repasObject = dayObject.getJSONObject(typeRepas[j]);
                    Repas repas = new Repas();
                    repas.setTypeRepas(typeRepas[j]);
                    repas.setJour(daysOfTheWeek[i]);
                    menu.addRepas(repas);


                    for (int k = 0; k < typeProduit.length ; k++) {
                        JSONArray produitArray = repasObject.getJSONArray(typeProduit[k]);

                        for (int l = 0; l < produitArray.length() ; l++) {
                            JSONObject produitObject = produitArray.getJSONObject(l);

                            String designation = produitObject.getString("designation");
                            String typeProduitt = typeProduit[k];

                            String description = produitObject.getString("description");

                            int bio = Integer.parseInt(produitObject.getString("bio"));
                            int local = Integer.parseInt(produitObject.getString("local"));
                            int maison = Integer.parseInt(produitObject.getString("maison"));
                            int frais = Integer.parseInt(produitObject.getString("frais"));


                            Produit produit = new Produit(
                                    designation,
                                    typeProduitt,
                                    description,
                                    bio,
                                    maison,
                                    local,
                                    frais
                            );

                            repas.addProduit(produit);

                        }


                    }

                }
                listToAddSomeContent.add(menu);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return listToAddSomeContent;
    }

}
