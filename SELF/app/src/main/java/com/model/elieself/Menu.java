package com.model.elieself;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Menu.
 */
public class Menu {
    private List<Repas> repas;
    private String semaine;

    /**
     * Instantiates a new Menu.
     *
     * @param repas   the repas
     * @param semaine the semaine
     */
    public Menu(List<Repas> repas, String semaine) {
        this.repas = repas;
        this.semaine = semaine;
    }

    /**
     * Instantiates a new Menu.
     */
    public Menu() {
        repas = new ArrayList<>();
    }

    /**
     * Add repas.
     *
     * @param repas the repas
     */
    public void addRepas(Repas repas){
        this.repas.add(repas);
    }

    /**
     * Sets semaine.
     *
     * @param semaine the semaine
     */
    public void setSemaine(String semaine) {
        this.semaine = semaine;
    }

    /**
     * Gets repas.
     *
     * @return the repas
     */
    public List<Repas> getRepas() {
        return repas;
    }

    /**
     * Gets semaine.
     *
     * @return the semaine
     */
    public String getSemaine() {
        return semaine;
    }

    @Override
    public String toString() {
        return "Menu{" +
                "repas=" + repas +
                ", semaine=" + semaine +
                '}';
    }
}
