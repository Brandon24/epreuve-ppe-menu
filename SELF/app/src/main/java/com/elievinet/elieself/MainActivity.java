package com.elievinet.elieself;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.customWidget.elieself.ListViewAdapter;
import com.dao.MenuDAO;
import com.dao.ProduitDAO;
import com.database.elieself.DatabaseHelper;
import com.date.elieself.CustomDate;
import com.model.elieself.Menu;
import com.model.elieself.Produit;
import com.parser.JsonParser;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Cette classe a pour but de récuperer les données du menu provenant du JSON et
 * de les afficher dans une ListView
 * Nous implémentons  l'interface OnItemSelectedListener pour pouvoir savoir quel jour choisit
 * l'utilisateur.
 */
public class MainActivity extends Activity implements AdapterView.OnItemSelectedListener {
    private ArrayList<Produit> listDesProduits = new ArrayList<>();
    private ListViewAdapter adapter;
    private ProduitDAO produitDAO;
    private MenuDAO menuDAO;
    private String urlData = "https://flappybat.fr/menu/php/getmenu.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        menuDAO = new MenuDAO(this);
        produitDAO = new ProduitDAO(this);

        if (isNetworkAvailable()){
            requestData();
        }else{
            updateListProduit(CustomDate.getCurrentDay());
            initComponents();
        }


    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /**
     * Récupére les données de l'url distante
     */
    private void requestData(){
        RequestQueue queue = Volley.newRequestQueue(this);

        StringRequest jsonObjectRequest = new StringRequest(
                Request.Method.POST, urlData,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        List<Menu> menuList = new ArrayList<>();
                        menuList = JsonParser.getMenusFromJson(response,menuList);
                        menuDAO.addMenuToDatabase(menuList);
                        updateListProduit(CustomDate.getCurrentDay());
                        initComponents();

                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error){
                        // Do something when error occurred
                        error.printStackTrace();
                    }
                }){
                @Override
                public byte[] getBody() throws AuthFailureError {
                    HashMap<String, String> params2 = new HashMap<String, String>();
    //                        params2.put("date", "2019-05-01");
                    return new JSONObject(params2).toString().getBytes();
                }

                @Override
                public String getBodyContentType() {
                    return "application/json";
                }};

// Add the request to the RequestQueue.
        queue.add(jsonObjectRequest);
    }

    private void initComponents(){
        TextView textViewSemaine = findViewById(R.id.textView3);
        textViewSemaine.setText(CustomDate.getMondayAndFriday());

        Spinner spinner = findViewById(R.id.spinner);
        ListView listView = findViewById(R.id.imagesListView);

        adapter = new ListViewAdapter(getApplicationContext(),R.layout.custom_layout_list, listDesProduits);

        ArrayAdapter<CharSequence> adapterSpinner;
        adapterSpinner = ArrayAdapter.createFromResource(getApplicationContext(),
                R.array.day_week, android.R.layout.simple_spinner_item);
        adapterSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapterSpinner);
        // on recupere la position du jour actuelle dans le tableau de lundi a vendredi
        spinner.setSelection(CustomDate.getDayPosition(CustomDate.getCurrentDay()));

        // Définition Evenement et attribution jour
        spinner.setOnItemSelectedListener(this);
        listView.setAdapter(adapter);

    }

    /**
     * Update list produit.
     *
     * @param jourVoulu est le jour cu l'on va récupérer les données du menu
     */
    public void updateListProduit(String jourVoulu){
        /**
         Aprés avoir reçu les données  du json et les avoir insérer dans la base
         on récupére les données de la base pour les injecter dans notre arraylist
         on va d'abord injecter le produit du midi
         On injecte un faux menu du a la contrainte de notre interface avec le listview
         Qui va nous permettre d'afficher l'entete DINER avant les résultats
         aprés on va injecter le repas du soi*/
        listDesProduits.clear();
        Produit produitDuMidi = new Produit();
        Produit produitDuSoir = new Produit();
        produitDuSoir.setDesignation("DINER");
        produitDuMidi.setDesignation("DEJEUNER");
        listDesProduits.add(produitDuMidi);
        listDesProduits.addAll(produitDAO.getAllProduitForOneDayWithSpecificTypeProduitAndTypeRepas("dejeuner",jourVoulu,"entree"));
        listDesProduits.addAll(produitDAO.getAllProduitForOneDayWithSpecificTypeProduitAndTypeRepas("dejeuner",jourVoulu,"plat"));
        listDesProduits.addAll(produitDAO.getAllProduitForOneDayWithSpecificTypeProduitAndTypeRepas("dejeuner",jourVoulu,"accompagnement"));
        listDesProduits.addAll(produitDAO.getAllProduitForOneDayWithSpecificTypeProduitAndTypeRepas("dejeuner",jourVoulu,"dessert"));
        listDesProduits.add(produitDuSoir);
        listDesProduits.addAll(produitDAO.getAllProduitForOneDayWithSpecificTypeProduitAndTypeRepas("diner",jourVoulu,"entree"));
        listDesProduits.addAll(produitDAO.getAllProduitForOneDayWithSpecificTypeProduitAndTypeRepas("diner",jourVoulu,"plat"));
        listDesProduits.addAll(produitDAO.getAllProduitForOneDayWithSpecificTypeProduitAndTypeRepas("diner",jourVoulu,"accompagnement"));
        listDesProduits.addAll(produitDAO.getAllProduitForOneDayWithSpecificTypeProduitAndTypeRepas("diner",jourVoulu,"dessert"));
    }

    /**
     *
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        listDesProduits.clear();
        String jourVoulu = CustomDate.daysOfTheWeek[position];
        updateListProduit(jourVoulu);
        adapter.notifyDataSetChanged();

    }

    /**
     *
     * @param parent
     */
    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }
}

