<?php
header('Access-Control-Allow-Origin: *');

$reponce = array('msg' => "", 'success' => true);
// ajouter condition de verification


if ($reponce['success']) {
	try {
		include('connectBDD.php');
		$prepared = $bdd->prepare("DELETE FROM `menu_repas_produit` WHERE `date` = ? AND `type_repas` = ? AND `designation` = ? AND `type_produit` = ?");
		$prepared->execute(array($_POST["date"], $_POST["type_repas"], $_POST["designation"], $_POST["type_produit"]));
		$reponce['msg'] = "Suppression reussi";
	} catch (Exception $e) {
		$reponce['msg'] = $e->getMessage();
		$reponce['success'] = false;
	}
}
echo json_encode($reponce, JSON_UNESCAPED_UNICODE);
?>
