<?php
header('Access-Control-Allow-Origin: *');

$reponce = array('msg' => "", 'success' => true);

session_start();
include("permission.php");

if (!hasPermission()) {
	$reponce['msg'] .= "Action non autorisé ";
	$reponce['success'] = false;
}



if (date("w", strtotime($_POST["date"])) > 5) {
	$reponce['msg'] .= "Repas hors date ";
	$reponce['success'] = false;
}
if($_POST["type_repas"] != "dejeuner" && $_POST["type_repas"] != "diner"){
	$reponce['msg'] .= "type de repas invalide";
	$reponce['success'] = false;
}
// ajouter condition de verification


if ($reponce['success']) {
	try {
		include('connectBDD.php');
		$prepared = $bdd->prepare("INSERT IGNORE INTO `menu_repas`(`date`, `type`) VALUES (?, ?)");
		$prepared->execute(array($_POST["date"], $_POST["type_repas"]));
		$prepared = $bdd->prepare("INSERT IGNORE INTO `menu_produit`(`designation`, `type`) VALUES (?, ?)");
		$prepared->execute(array($_POST["designation"], $_POST["type_produit"]));
		$prepared = $bdd->prepare("INSERT INTO `menu_repas_produit`(`date`, `type_repas`, `designation`, `type_produit`, `bio`, `local`, `maison`, `frais`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
		$prepared->execute(array($_POST["date"], $_POST["type_repas"], $_POST["designation"], $_POST["type_produit"], $_POST["bio"], $_POST["local"], $_POST["maison"], $_POST["frais"]));
		$reponce['msg'] = "Ajout reussi";
	} catch (Exception $e) {
		$reponce['msg'] = $e->getMessage();
		$reponce['success'] = false;
	}
}
$reponce .= "bio=".$_POST["bio"];
echo json_encode($reponce, JSON_UNESCAPED_UNICODE);
?>
